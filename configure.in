dnl Process this file with autoconf to produce a configure script.
AC_INIT(src/gallery.c)
AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE(gphoto, 0.5.8)
AM_MAINTAINER_MODE

dnl Pick up the Gnome macros.
AM_ACLOCAL_INCLUDE(macros)

dnl Make --disable-static the default
AC_DISABLE_STATIC

dnl Checks for programs.
AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CPP
AC_STDC_HEADERS
AC_ARG_PROGRAM
AM_PROG_LIBTOOL

dnl GNOME checks
GNOME_INIT
GNOME_COMPILE_WARNINGS
GNOME_X_CHECKS

dnl Completed by AM_INIT_AUTOMAKE
dnl AC_PROG_INSTALL
dnl AC_PROG_MAKE_SET

dnl Add the languages which your application supports here.
ALL_LINGUAS="az ca da de fr hu it ja nl no pl pt_BR ru sl sv uk"
AM_GNU_GETTEXT

dnl Set PACKAGE_LOCALE_DIR in config.h.
if test "x${prefix}" = "xNONE"; then
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${ac_default_prefix}/${DATADIRNAME}/locale")
else
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${prefix}/${DATADIRNAME}/locale")
fi

dnl Checks for libraries.

dnl Check -ldl
AC_CHECK_LIB(dl, dlopen)

dnl Check for X
AC_PATH_X

dnl Check -ljpeg:
AC_CHECK_LIB(jpeg, jpeg_start_decompress)

dnl Check for glib
AM_PATH_GLIB(1.2.6)

dnl Check for gtk
AM_PATH_GTK(1.2.6)

dnl Check for gdk_imlib
AM_PATH_GDK_IMLIB(1.9.0)

dnl Check for gdk-pixbuf
AM_PATH_GDK_PIXBUF(0.9.0, ,AC_MSG_ERROR([Need gdk-pixbuf-0.9.0 or later!]))

dnl Check for GnomePrint
AC_MSG_CHECKING(for GnomePrint libraries >= 0.25)
if gnome-config --libs print > /dev/null 2>&1; then
    vers=`gnome-config --modversion print | sed -e "s/gnome-print-//" | \
        awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
    if test "$vers" -ge 25; then
        AC_MSG_RESULT(found)
	CFLAGS="$CFLAGS `gnome-config --cflags print`"
	LIBS="$LIBS `gnome-config --libs print`"
    else
        AC_MSG_ERROR(You need at least GNOME print 0.25)
    fi
else
    AC_MSG_ERROR(Did not find GnomePrint installed)
fi

dnl libgphoto2
dnl **********
AC_MSG_CHECKING(for libgphoto2 >= 2.0)
if gnome-config --libs libgphoto2 > /dev/null 2>&1; then
    AC_MSG_RESULT(found)
    GPHOTO2_CFLAGS="`gphoto2-config --cflags`"
    GPHOTO2_LIBS="`gphoto2-config --libs`"
else
    AC_MSG_ERROR([
*** You need libgphoto2 for this version of ${PACKAGE}.
*** Get it from http://sourceforge.net/projects/gphoto/])
fi
AC_SUBST(GPHOTO2_CFLAGS)
AC_SUBST(GPHOTO2_LIBS)

dnl Solaris needs posix4 library
changequote(, )dnl
if test -n "`echo $host_os | grep [sS]olaris`"; then
  LIBS="$LIBS -lposix4"
fi
changequote([, ])dnl

AC_SUBST(CFLAGS)
AC_SUBST(LIBS)

dnl Checks for header files.
AC_HEADER_STDC
AC_HEADER_DIRENT
AC_CHECK_HEADERS(sys/param.h termios.h sgetty.h ttold.h ioctl-types.h \
	fcntl.h sgtty.h sys/ioctl.h sys/time.h termio.h unistd.h \
	endian.h byteswap.h asm/io.h getopt.h)

dnl Checks for typedefs, structures, and compiler characteristics.
AC_C_INLINE
AC_C_CONST
AC_TYPE_UID_T
AC_TYPE_OFF_T
AC_TYPE_SIZE_T
AC_HEADER_TIME
AC_STRUCT_TM

dnl Check if TIOCM_RTS is included in one of several possible files
AC_TRY_COMPILE([#include <termios.h>], [int foo = TIOCM_RTS;], 	
			AC_DEFINE(HAVE_RTS_IOCTL))
AC_TRY_COMPILE([#include <termio.h>], [int foo = TIOCM_RTS;], 	
			AC_DEFINE(HAVE_RTS_IOCTL))
AC_TRY_COMPILE([#include <ioctl-types.h>], [int foo = TIOCM_RTS;], 	
			AC_DEFINE(HAVE_RTS_IOCTL))
AC_TRY_COMPILE([#include <sys/ioctl.h>], [int foo = TIOCM_RTS;], 	
			AC_DEFINE(HAVE_RTS_IOCTL))

dnl Checks for library functions.
AC_PROG_GCC_TRADITIONAL
AC_FUNC_MEMCMP
AC_FUNC_STRFTIME
AC_FUNC_VPRINTF
AC_CHECK_FUNCS(setreuid mkdir mktime strcspn strdup strerror strtol select \
	getopt_long getopt)

AC_PATH_PROG(LIBUSB_CONFIG,libusb-config)
if test -n "${LIBUSB_CONFIG}"; then
  CFLAGS="$CFLAGS `$LIBUSB_CONFIG --cflags`"
  GPIO_CFLAGS="$GPIO_CFLAGS `$LIBUSB_CONFIG --cflags`"
  LIBS="$LIBS `$LIBUSB_CONFIG --libs`"
  GPIO_USB=1
else
  GPIO_USB=0
fi

AM_CONDITIONAL(GPIO_USB, test $GPIO_USB = "1")
AC_SUBST(GPIO_USB)

dnl Local setup
dnl Is there a better way of doing docs?
GPHOTO_EXPAND_DIR(DOCDIR, "$datadir/gphoto/doc")
AC_DEFINE_UNQUOTED(DOCDIR, "$DOCDIR")

dnl Or gallery themes?
GPHOTO_EXPAND_DIR(GALLERYDIR, "$datadir/gphoto/gallery")
AC_DEFINE_UNQUOTED(GALLERYDIR, "$GALLERYDIR")

driversdir='${pkglibdir}'
AC_SUBST(driversdir)

gallerydir='${pkgdatadir}/gallery'
AC_SUBST(gallerydir)

AC_OUTPUT([
gphoto.spec
Makefile
intl/Makefile
po/Makefile.in
barbie/Makefile
canon/Makefile
casio/Makefile
digita/Makefile
dimera/Makefile
directory/Makefile
fuji/Makefile
gallery/Makefile
gallery/CSStheme/Makefile
gallery/CSSjimmac/Makefile
gallery/Default/Makefile
gallery/RedNGray/Makefile
kodak/Makefile
kodak/dc21x/Makefile
kodak_generic/Makefile
konica/Makefile
konica_qmxxx/Makefile
man/Makefile
minolta/Makefile
mustek/Makefile
nikon/Makefile
philips/Makefile
photopc/Makefile
jd11/Makefile
polaroid/Makefile
ricoh/Makefile
samsung/Makefile
scripts/Makefile
sony/Makefile
sony/dscf55/Makefile
src/Makefile
src/icons/Makefile
])
