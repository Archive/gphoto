#include <config.h>
#include <gnome.h>

#include "gphoto-gnome.h"
#include "gphoto-config.h"

GPhotoCfg *config = NULL;

void
gphoto_config_load (void)
{
	gboolean do_set;
	
	if (!config) {
		config = g_malloc (sizeof (GPhotoCfg));
	}


	gnome_config_push_prefix ("/gnome-photo/General/");

	config->run = gnome_config_get_int ("run");

	if (!config->run) {
		g_print ("First-time run.\n");
	}
	gnome_config_push_prefix ("/gnome-photo/Device/");
#ifdef __NetBSD__
	config->port = gnome_config_get_string_with_default ("port=/dev/tty00", &do_set);
#else
	config->port = gnome_config_get_string_with_default ("port=/dev/ttyS0", &do_set);
#endif
	config->name = gnome_config_get_string_with_default ("name=vfs", &do_set);
	/* Default type is GPHOTO_CAMERA_NONE */
	config->type = gnome_config_get_int_with_default("type=0", &do_set);

	config->cli = gnome_config_get_int_with_default("cli=0", &do_set);
	config->script = gnome_config_get_string_with_default("script=", &do_set);

	config->mdi = gnome_config_get_int_with_default("mdi=0", &do_set);

	gnome_config_pop_prefix();
	gnome_config_sync();
}

void
gphoto_config_save (void)
{
	gnome_config_push_prefix ("/gnome-photo/General/");

	if (!config->run) {
		config->run = TRUE;
		gnome_config_set_int ("run", (gint) config->run);
	}

	gnome_config_push_prefix ("/gnome-photo/Device/");
	gnome_config_set_string ("port", config->port);
	gnome_config_set_string ("name", config->name);
	gnome_config_set_int ("type", config->type);

	gnome_config_push_prefix ("/gnome-photo/Interface/");
	gnome_config_set_int ("cli", config->cli);
	gnome_config_set_string ("script", config->script);
	gnome_config_set_int ("mdi", config->mdi);

	gnome_config_pop_prefix();
	gnome_config_sync();
}

	
