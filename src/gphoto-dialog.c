#include <config.h>
#include <gnome.h>

#include "gphoto-gnome.h"
#include "gphoto-dialog.h"

#include <libgnomeprint/gnome-printer.h>
#include <libgnomeprint/gnome-printer-dialog.h>
#include <libgnomeprint/gnome-print-master.h>
#include <libgnomeprint/gnome-print-master-preview.h>
#include <libgnomeprint/gnome-print-preview.h>

void
gphoto_dialog_about_cb (void)
{
	GtkWidget *about;

	const gchar *authors[] = {
		N_("Ole Aamot <ole@gnu.org>"),
		NULL
	};

#ifdef ENABLE_NLS
	{
		int i;

		for (i = 0; authors[i] != NULL; i++) {
			authors [i] = _(authors [i]);
		}
	}
#endif
	about = gnome_about_new (_("GNOME Photo"), GPHOTO_VERSION,
				 _("(C) 2001  Free Software Foundation, Inc."),
				 authors,
				 NULL,
				 NULL);
	gtk_widget_show (about);
}
		
gboolean
gphoto_dialog_print_cb (void) /*  PrintJobInfo *pji) */
{
	GnomeDialog *dialog;

	dialog = (GnomeDialog *) gnome_print_dialog_new(
		(const char *) _("GNOME Photo Print"),
		GNOME_PRINT_DIALOG_RANGE);

	switch (gnome_dialog_run (GNOME_DIALOG(dialog))) {
	case GNOME_PRINT_PRINT:
		break;
	case GNOME_PRINT_PREVIEW:
/*  		pji->preview = TRUE; */
		break;
	case -1:
		return TRUE;
	default:
		gnome_dialog_close (GNOME_DIALOG(dialog));
		return TRUE;
	}
}
