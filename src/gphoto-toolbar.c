#include <config.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnome/gnome-i18n.h>

#include "gphoto-callbacks.h"

static GnomeUIInfo gphoto_toolbar [] = {
	GNOMEUIINFO_ITEM_STOCK (N_("Open"), N_("Open Image"), gp_open_dialog_cb,
				GNOME_STOCK_PIXMAP_OPEN),
	GNOMEUIINFO_ITEM_STOCK (N_("Save"), N_("Save Image"), gp_save_dialog_cb,
				GNOME_STOCK_PIXMAP_SAVE),
	GNOMEUIINFO_ITEM_STOCK (N_("Print"), N_("Print Image"), gp_print_dialog_cb,
				GNOME_STOCK_PIXMAP_PRINT),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_STOCK (N_("Exit"), N_("Exit GPhoto"), gtk_main_quit,
				GNOME_STOCK_PIXMAP_EXIT),
	GNOMEUIINFO_END
};

void
gphoto_install_toolbar(GtkWidget *app)
{
	gnome_app_create_toolbar_with_data(GNOME_APP(app), gphoto_toolbar, app);
}
