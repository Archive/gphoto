#ifndef __GPHOTO_DIALOG_H__
#define __GPHOTO_DIALOG_H__

void gphoto_dialog_about_cb (void);
void gphoto_dialog_prefs_cb (void);
gboolean gphoto_dialog_print_cb (void);

#endif /* __GPHOTO_DIALOG_H__ */
