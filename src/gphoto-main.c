#include <config.h>
#include <gnome.h>

#ifdef HAVE_GUILE
#  include <libguile.h>
#endif

#ifdef ENABLE_BONOBO
#  include <bonobo.h>
#endif

#include "gphoto-gnome.h"
#include "gphoto-config.h"
#include "gphoto-debug.h"
#include "gphoto-menu.h"
#include "gphoto-toolbar.h"

gint debug = 0;
gint debug_canvas = 0;
gint debug_config = 0;
gint debug_delete = 0;
gint debug_dialog = 0;
gint debug_format = 0;
gint debug_gallery = 0;
gint debug_get_index = 0;
gint debug_get_image = 0;
gint debug_get_thumb = 0;
gint debug_index = 0;
gint debug_init = 0;
gint debug_live = 0;
gint debug_load_cfg = 0;
gint debug_load_img = 0;
gint debug_next = 0;
gint debug_prefs = 0;
gint debug_prev = 0;
gint debug_print = 0;
gint debug_recent = 0;
gint debug_save_cfg = 0;
gint debug_save_img = 0;
gint debug_scale = 0;
gint debug_session = 0;
gint debug_set_model = 0;
gint debug_summary = 0;
gint debug_view = 0;
gint debug_window = 0;

/* Old cruft from zero dot four */

char *serial_port = NULL;

static const struct poptOption options[] = {
	{"debug", '\0', 0, &debug, 0, N_("Turn on debug messages."), NULL},
	{NULL, '\0', 0, NULL, 0}
};

gint
gphoto_main_quit ()
{
	gphoto_debug(DEBUG_INIT, N_("Almost exiting..."));
	/* FIXME Save config and vital data */
	gphoto_config_save();
	gtk_main_quit();
	return 0;
}

int
main (int argc, char *argv[])
{
	int i;
	char **args;
	poptContext ctx;
	GList *filelist = NULL;

	GtkWidget *main_app;
	GtkWidget *frame;
	GtkWidget *status;

	GPhotoCfg *config;

	gphoto_debug (DEBUG_INIT, "Starting gphoto...");

#ifdef ENABLE_NLS
	bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain (PACKAGE);
#endif

	gnome_init_with_popt_table ("gphoto", GPHOTO_VERSION, argc, argv,
				    options, 0, &ctx);

	args = (char **) poptGetArgs (ctx);

	for (i = 0; args && args[i]; i++)
		filelist = g_list_append (filelist, args[i]);
	poptFreeContext (ctx);

	gphoto_debug (DEBUG_INIT, "Initializing libraries...");

	main_app = gnome_app_new (PACKAGE, N_("GNOME Photo"));

	frame = gtk_frame_new(NULL);

	gtk_window_set_policy (GTK_WINDOW (main_app), FALSE, TRUE, FALSE);
	gtk_window_set_default_size (GTK_WINDOW (main_app), 250, 450);

	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);

	gnome_app_set_contents (GNOME_APP (main_app), frame);

	status = gnome_appbar_new (FALSE, TRUE, GNOME_PREFERENCES_NEVER);
	gnome_app_set_statusbar (GNOME_APP (main_app), status);
	
	gphoto_install_menu (main_app);
	gphoto_install_toolbar (main_app);

	gtk_widget_show (main_app);
	
	gtk_main ();

	gphoto_debug (DEBUG_INIT, "Qut of gtk_main...");

	return (0);
}
