#ifndef __GPHOTO_CONFIG_H__
#define __GPHOTO_CONFIG_H__

typedef struct _GPhotoCfg GPhotoCfg;

struct _GPhotoCfg
{
	gchar *port;
	gchar *name;
	gchar *script;
	gint   type;
	gint   cli;
	gint   mdi;
	gint   run;
};

extern GPhotoCfg *config;

void gphoto_config_load (void);
void gphoto_config_save (void);

#endif /* __GPHOTO_CONFIG_H__ */

