#include <config.h>
#include <glib.h>

#include "gphoto-gnome.h"
#include "gphoto-callbacks.h"

gint gp_connect_device_cb (void)
{
	return GP_OK;
}

void gp_open_dialog_cb (void)
{
}

void gp_save_dialog_cb (void)
{
}

void gp_print_dialog_cb (void)
{
}

void gp_preview_dialog_cb (void)
{
}

void gp_save_gallery_cb (void)
{
}

void gp_rotate_dialog_cb (void)
{
}

void gp_resize_dialog_cb (void)
{
}

void gp_crop_dialog_cb (void)
{
}

void gp_select_all_cb (void)
{
}

void gp_select_none_cb (void)
{
}

void gp_select_close_cb (void)
{
}

void gp_select_save_cb (void)
{
}

void update_status (void)
{
}

void update_progress (void)
{
}
