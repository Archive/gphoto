#include <config.h>
#include <gnome.h>

#include "gphoto-debug.h"
#include "gphoto-recent.h"

#ifndef DATA_ITEMS_TO_REMOVE
#define DATA_ITEMS_TO_REMOVE "items_to_remove"
#endif

static void  gphoto_recent_update_menus (GnomeApp *app, GList *recent_files);
static void  gphoto_recent_cb (GtkWidget *w, gpointer data);
static GList *gphoto_recent_history_get_list (void);
static gchar *gphoto_recent_history_update_list (const gchar *filename);

GList *gphoto_recent_history_list = NULL;

static GList*
gphoto_recent_history_get_list (void)
{
	gchar *filename, *key;
	gint max_entries, i;
	gboolean do_set = FALSE;

	gphoto_debug (DEBUG_RECENT, "");

	if (gphoto_recent_history_list)
		return gphoto_recent_history_list;

	gnome_config_push_prefix ("/gnome-photo/History/");

	/* Get max number of history entries.
	   Write default value to config file if no entry exists. */

	max_entries = gnome_config_get_int_with_default ("MaxFiles=4", &do_set);

	if (do_set)
		gnome_config_set_int ("MaxFiles", 4);

	for (i=0; i<max_entries; i++) {
		key = g_strdup_printf ("File%d", i);
		filename = gnome_config_get_string (key);
		if (filename == NULL) {
			/* Ran out of filenames */
			g_free (key);
			break;
		}
		gphoto_recent_history_list = g_list_append (gphoto_recent_history_list, filename);
		g_free (key);
	}
	gnome_config_pop_prefix ();

	return gphoto_recent_history_list;
}
