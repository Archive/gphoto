#ifndef __GPHOTO_RECENT_H__
#define __GPHOTO_RECENT_H__

void gphoto_recent_update (GnomeApp *app);
void gphoto_recent_update_all_windows (GnomeMDI *mdi);
void gphoto_recent_add (const gchar *filename);
void gphoto_recent_remove (GnomeApp *app);
void gphoto_recent_history_write_config (void);

#endif /* __GPHOTO_RECENT_H__ */
