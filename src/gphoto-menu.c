#include <config.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnome/gnome-i18n.h>

#include "gphoto-callbacks.h"
#include "gphoto-dialog.h"

static GnomeUIInfo file_menu [] =
{
	{
		GNOME_APP_UI_ITEM, N_("C_onnect..."),
		N_("Open digital camera connection"),
		gp_connect_device_cb, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_JUMP_TO,
		0, 0, NULL
	},
	GNOMEUIINFO_MENU_OPEN_ITEM(gp_open_dialog_cb, NULL),
	GNOMEUIINFO_ITEM_STOCK (N_("Open from _URI..."),
				N_("Open a file from a specified URI"),
				NULL,
				GNOME_STOCK_MENU_OPEN),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_SAVE_ITEM(gp_save_dialog_cb, N_("Save all")),
	GNOMEUIINFO_MENU_SAVE_ITEM(gp_select_save_cb, N_("Save selected")),

	/* Recent files ... */
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_NONE(N_("Export HTML gallery"),
			      N_("Export album"),
			      gp_save_gallery_cb),
	GNOMEUIINFO_ITEM_NONE(N_("Print"),
			      N_("Print contact sheet"),
			      gp_print_dialog_cb),
	GNOMEUIINFO_ITEM_NONE(N_("Print preview..."),
			      N_("Preview data to be printed"),
			      gp_preview_dialog_cb),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_CLOSE_ITEM (gp_select_close_cb, NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_EXIT_ITEM(gphoto_main_quit, N_("_Exit")),
	GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[] =
{
	GNOMEUIINFO_ITEM_NONE (N_("Crop"),
			       N_("Crop image"),
			       gp_crop_dialog_cb),
	GNOMEUIINFO_ITEM_NONE (N_("Resize..."),
			       N_("Resize image"),
			       gp_resize_dialog_cb),
	GNOMEUIINFO_ITEM_NONE (N_("Rotate..."),
			       N_("Rotate image"),
			       gp_rotate_dialog_cb),
	GNOMEUIINFO_MENU_SELECT_ALL_ITEM(gp_select_all_cb, NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo settings_menu[] =
{
	GNOMEUIINFO_MENU_NEW_ITEM (N_("_Camera Settings"),
				   N_("View/Set Camera Preferences"),
				   NULL, NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo windows_menu[] =
{
	GNOMEUIINFO_END
};



GnomeUIInfo help_menu [] =
{
        GNOMEUIINFO_HELP ("GPhoto"),
	GNOMEUIINFO_MENU_ABOUT_ITEM (gphoto_dialog_about_cb, NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo gphoto_menu[] =
{
	GNOMEUIINFO_MENU_FILE_TREE (file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE (edit_menu),
	GNOMEUIINFO_MENU_SETTINGS_TREE (settings_menu),
	GNOMEUIINFO_MENU_WINDOWS_TREE (windows_menu),
	GNOMEUIINFO_MENU_HELP_TREE (help_menu),
	GNOMEUIINFO_END
};


void
gphoto_install_menu (GtkWidget *app)
{
	gnome_app_create_menus_with_data(GNOME_APP(app), gphoto_menu, app);
	gnome_app_install_menu_hints(GNOME_APP(app), gphoto_menu);
}

