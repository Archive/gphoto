
gPhoto 0.4 Benutzeranleitung

Inhaltsverzeichnis

Einstellung von gPhoto
Kamerabenutzung
Einen Kleinbildindex bekommen
Laden von Bildern/Kleinbildern
Bilder von der Kamera l�schen
Bilder mit gPhoto aufnehmen
Konfiguration der Kamera
Eine Inhaltsangabe von der Kamera bekommen
Verzeichnisse durchsehen
Bilder bearbeiten
Bildausrichtung
Bildma�e
Nachbearbeitung
Ge�ffnete Bilder speichern
Bilder drucken
Plug-Ins
Live Camera!
Kommandozeilenmodus

Einstellung von gPhoto

Wenn Sie gPhoto zum erstenmal starten, sehen Sie einen Dialog, in diesem 
k�nnen Sie dann den seriellen Port und das Kameramodell ausw�hlen. W�hlen Sie 
Ihre Kamera und den seriellen Port, an dem die Kamera angeschlossen ist, dann 
klicken Sie "save" um die Informationen zu speichern. Wenn Sie diese 
Einstellungen sp�ter �ndern m�chten, gehen Sie in das "Configure" Men� und 
w�hlen "Select Port-Camera Model".

Kamerabenutzung

Einen Kleinbildindex bekommen:
Der Kleinbildindex ist eine Vorschau der vorhandenen Bilder. Aus diesem Index 
k�nnen Sie w�hlen welche Bilder Sie auf den PC laden m�chten. In gPhoto ist 
es m�glich zwei verschiedene Arten von Index zu bekommen.

Kleinbilder(thumbnails):
Die Kleinbilder tauchen als Index auf. Dieser erm�glicht es Ihnen die 
verf�gbaren Bilder zu sehen und auszuw�hlen. Durch einmaliges Klicken auf 
das Kleinbild wird es markiert. Zweilmaiges Klicken bewirkt, dass das Bild 
sofort geladen wird.

Keine Kleinbilder(no thumbnails):
Dies ist um einiges schneller als einen Kleinbildindex zu holen und 
handlicher, wenn man nur einige Bilder von der Kamera laden m�chte. F�r 
jedes Bild auf der Kamera wird ein Button im Index angezeigt, ohne 
Kleinbild. Um das Kleinbild zu sehen, m�ssen Sie auf den leeren Button 
doppelklicken, durch nochmaliges doppelklicken wird das Bild 
heruntergeladen. Sie k�nnen Bilder auch durch einfaches Klicken auf den 
Button markieren.

Diese Optionen sind im "Camera" Men� unter "Download Index" und auch als 
Buttons verf�gbar.

Laden von Bildern/Kleinbildern
Wenn Sie die Bilder, die Sie von der Kamera laden m�chten, im Index 
ausgew�hlt haben, k�nnen Sie die Bilder und/oder Kleinbilder auf zwei 
verschiedenen Wegen von der Kamera holen.

In einem Fenster �ffnen:
"Camera -> Download selected -> Images -> Open in window"
Dies l�dt die ausgew�hlten Bilder in das gPhoto Fenster. Sie k�nnen die 
Bilder dann ver�ndern und speichern. Die Bilder k�nnen auch in anderen 
Formaten gespeichert werden. Eine Anmerkung f�r professionelle Photographen: 
Die EXIF Tags werden nicht ver�ndert.

Speichern auf der Festplatte
"Camera -> Download selected -> Images -> Save to disk"
Dies speichert die von Ihnen ausgew�hlten Bilder auf der Festplatte. Sie 
werden nach dem Verzeichnis gefragt, in dem die Sie die Bilder speichern 
wollen und nach dem Dateinamenspr�fix. Die Bildnummer wird bei der 
Speicherung an das Pr�fix angeh�ngt. Zum Beispiel: wenn Sie die Bilder 1 und 
2 ausw�hlen, um sie auf der Festplatte zu speichern, und Sie haben den 
Anfang " MeineKatze" eingegeben, dann w�rden die Bilder als 
"MeineKatze-001.jpg" und "MeineKatze-002.jpg" gespeichert werden. (das 
Format h�ngt von der Kamera ab)

Es ist m�glich die Bilder f�r einen Index abzuspeichern. Diese Optionen 
finden Sie im "Camera" Men�, unter "Download Selected". Das "Images" Men� 
dort speichert nur die Bilder; das "thumbnails" Men� speichert nur die 
Kleinbilder, und das "Both" Men� speichert beides die Bilder UND die 
Kleinbilder.

Bilder von der Kamera l�schen:
Um Bilder von der Kamera zu l�schen m�ssen Sie sich erst den Index holen, und 
dann die Bilder, die zu l�schen sind ausw�hlen. Im "Camera" Men� w�hlen Sie 
"Delete Selected Images". Sie werden dann gefragt ob Sie die Bilder wirklich 
l�schen wollen. Es gibt auch einen Button in der Werkzeugleisteum die 
ausgew�hlten Bilder zu l�schen.

Bitte beachten Sie: manche Kameras verbieten die L�schung der Bilder durch 
ein externes Programm.

Bilder mit gPhoto aufnehmen
Im "Camera" Men� k�nnen Sie mit "Take Picture" die Kamera anweisen ein Bild 
aufzunehmen.

Bitte beachten Sie: manche Kameras verbieten es, dass Bilder durch ein 
externes Programm aufgenommen werden. Ihnen wird dann ein Fehlerfenster 
angezeigt.

Konfiguration der Kamera:
Aus dem "Configure" Men� k�nnen Sie "Configure Camera" ausw�hlen, um 
verschiedene Einstellungen der Kamera zu �ndern. Dies ist stark abh�ngig von 
der Kamera und wird unterschiedliche Ergebnisse mit verschiedenen Kameras 
bringen.

Eine Statusangabe von der Kamera bekommen:
"Camera -> Summary"
Eine Inhaltsangabe Ihrer Kamera wird Ihnen eine kurze Beschreibung des Status 
Ihrer Kamera ausgeben. Zum Beispiel, es k�nnte die Anzahl der geschossenen 
Bilder anzeigen, die Anzahl der Bilder, die Sie noch schie�en k�nnen, den 
Bateriestatus und so weiter. Diese Funktion h�ngt stark von der Kamera ab, 
deshalb werden unterschiedliche Kameras verschieden viele Informatonen 
hergeben.

Verzeichnisse durchsehen

Es ist in gPhoto m�glich ganze Verzeichnisse mit Bildern anzeigen zu lassen. 
Im "File" Men� w�hln Sie die Option " Open -> Directory...". Dann k�nnen Sie 
einen Index der Bilder in dem Verzeichnis sehen.

Eine wichtige Sache ist, dass das Verzeichnisseansehen genau wie das 
Bilderholen, von der Kamera, funktioniert. Sie w�hlen die Bilder aus und 
klicken auf "Download Selected" im "Camera" Men�, oder aus der Buttonleiste, 
um die ausgew�hlenten Bilder in dem Verzeichnis zu �ffnen. Sie k�nnen ein 
anderes Verzeichnis �ffnen indem Sie im "Camera" Men� "Download Index" w�hlen, 
oder den Button aus der Werkzeugleiste anklicken.

Bilder bearbeiten

Um ein Bild zu bearbeiten m�ssen Sie das Bild erst in gPhoto �ffnen. Dies 
k�nnen Sie entweder indem Sie das Bild von der Kamera holen und die Option 
"Open in window" anw�hlen, oder indem Sie das Bild, mit der "Open" Option aus 
dem "File" Men�, �ffnen.

Im "Image" Men� sehen Sie zwei verschiedene Optionen um ge�ffnete Bilder zu 
bearbeiten.

Bildausrichtung("orientation"):
Option zum drehen (mit dem Uhrzeigersinn , gegen den Uhrzeigersinn), und 
kippen 
(horizontal, vertikal).

Bildma�e("size"):
Option zum skalieren. Sie k�nnen entweder die schnelle Skalierung zur halben 
oder doppelten Gr��e nehmen, oder auf "Scale" klicken und Ihre eigene Gr��e 
angeben. Im Skalierungsdialog k�nnen Sie die genaue H�he und Breite, und 
die Option "Constrain Proportions" um die Proportion beizubehalten, w�hlen.

Es existieren Buttons in der Werkzeugleiste f�r alle obengenannten Optionen, 
bis auf  zwei,  halbe/doppelte Gr��e.

Nachbearbeitung

Die Nachbearbeitung l��t Ihnen ausgedehnte Bearbeitungs-M�glichkeiten. Wenn 
die Nachbearbeitung aktiviert ist wird nach dem holen jedes Bildes ein Script 
zur Bearbeitung gestartet. Drehung, Gr��en�nderung, Gammakorrektur und vieles 
mehr. Die Scripte werden automatisch auf jedes Bild, das von der Kamera kommt, 
angewendet, allerdings bevor es ge�ffnet oder gespeichrt wurde.

Klicken Sie auf "Post-Process", unten im Hauptfenster von gPhoto, um den 
Nachbearbeitungsdialog zu sehen. Sie k�nnen die Nachbearbeitung an- oder 
ausstellen indem Sie auf "Enable Post-Processing" klicken. In der Textbox 
k�nnen Sie die Kommandozeile eingeben, die auf jedes geholte Bild angewendet
wird. Der Name des jeweiligen Bildes wird durch "%s" repr�sentiert.

Bespiel:
Wenn Sie ein Skript in /usr/local/bin haben, welches die Gradzahl zum drehen 
und den Bildnamen als Argumente bekommt, k�nnen Sie das zur Nachbearbeitung 
benutzen, um alle geholten Bilder um 90� zu drehen:

/usr/local/bin/rotate 90 %s

(bei der Ausf�hrung wird das "%s" durch den vollst�ndigen Pfad und den Namen 
des Bildes ersetzt)

Tipp: ImageMagick ist breits installiert, wenn gPhoto auf Ihrem System l�uft. 
ImageMagick enth�lt eine Vielzahl an n�tzlichen Werkzeugen f�r die 
Bildbearbeitung und -manipulation und das bemerkenswerte "mogrify". Geben Sie 
"man mogrify" in einem Terminal ein, wenn Sie mehr Informationen wollen. Sie
k�nnen sehr viel mit diesem Werkzeug machen, zum Beispiel �nderung von Datum 
und Zeit, Gr��en�nderung und so weiter.

Bemerkung f�r Autoren: das Skript muss die Datei an ihrem Speicherort 
ver�ndern, weil gPhoto die gleiche Datei nach der Nachbearbeitung wieder 
�ffnet, wenn "Open in window" angew�hlt ist.


Ge�ffnete Bilder speichern

Sie k�nnen w�hlen ob Sie eins oder alle der ge�ffneten Bilder speichern 
wollen. Im "File" Men� w�hlen Sie "Save Opened Image(s)". F�r diese Option 
existiert auch ein Button.

W�hlen Sie im Dialog ein Verzeichnis in dem das/die Bild(er) gespeichert 
werden soll(en).

Wenn Sie nur das Bild, das Sie im Moment ansehen, speichern wollen, tippen Sie 
den vollen Pfad und Dateinamen ein, um das Bild mit "Save as" zu speichern. 
Sie k�nnen verschiedene Endungen eintippen um das Format der Datei 
festzulegen, das meint, dass, wenn Sie ein GIF-Bild speichern wollen, geben 
Sie ein .gif als Endung ein, f�r JPEG-Dateien benutzen Sie die Endung .jpg, 
und so weiter.

Wenn Sie alle ge�ffneten Dateien speichern wollen w�hlen Sie "Save all opened 
images", und geben einen Dateinamenspr�fix ein. Der Dateinamenspr�fix wird mit 
der Nummer des Bildes zusammen abgespeichert. Zum Beispiel: sie haben den 
Dateinamensanfang " MeineKatze" eingegeben, dannw�rden die erstenbeiden 
Bilder als "MeineKatze-001.jpg" und "MeineKatze-002.jpg" gespeichert werden.

Bilder ausdrucken

Bilder ausdrucken ist einfach. �ffnen Sie das Bild, das Sie drucken m�chten in 
gPhoto und w�hlen Sie dann, im "File" Men�, "Print" aus. F�r diese Option gibt 
es auch einen Button in der Werkzeugleiste.

Im "Print Image"-Dialog k�nnen Sie den Befehl, der "lpr" �bergeben wird um das 
Bild auszudrucken, selbst eingeben. Der Name der tempor�ren Datei wird am Ende 
des Kommandos angef�gt, benutzen Sie "-r" um die tempor�re Datei nach dem 
erfolgreichen Ausdruck zu l�schen. Wenn Sie die Option "-r" nicht �bergeben, 
behalten Sie das Bild in Ihrem $HOME/.gphoto Verzeichnis. Sie k�nnen auch 
weitere Argumente und Optionen an "lpr" �bergeben. Um mehr �ber die Optionen 
von "lpr" zu erfahren lesen Sie die Manpage, mit "man lpr".

Das Arbeiten mit lpr ist au�erhalb des Rahmens dieses Dokumentes. Der 
Druckbefehl der standardm��ig in gPhoto eingetragen ist sollte auf den meisten 
Systemen funktionieren. F�r mehr Informationen zu lpr und anderen 
linuxbetreffenden Druckfragen, lesen Sie bitte die PRINTING-HOWTO.

Plug-Ins

Live Camera!:
Dies erlaubt es Ihnen auf dem Monitor zu sehen, was die Kamera im Moment 
sieht. Wenn Sie den Button, den das Plug-In anbietet, anklicken, schie�t die 
Kamera ein Bild.

Wenn Sie Informationen m�chten, wie Sie Ihr eigenes "Theme" kreiren k�nnen, 
m�chten besuchen Sie http://www.gphoto.org/docs.php3 und sehen Sie sich die 
"HTML Gallery engine specs" an.

Kommandozeilenmodus

Der Kommandozeilenmodus erlaubt es Ihnen, gPhoto �ber Shellskripte gew�hliche 
Aufgaben ausf�hren zu lassen. Eine Sache, die Sie wissen sollten, ist, dass 
gPhoto einen laufenden X-Server braucht um die Bilder zu bekommen. Auch die 
Verzeichnisinhaltsansicht und die Kamerakonfiguration werden nicht
funktionieren.

Unten ist eine der verf�gbaren Optionen und ein paar Beispiele.

Usage: gphoto [-h] [-n] [-s # filename] [-t # filename]
 [-d #] [-l filename]
-nAnzeige von Bild #
-s # filename speichere Bild # als filename
-t # filename speichere Kleinbild # als filename
-d # l�scht Bild # von der Kamera
-l filename speichere Vorschau als filename
-h zeige dieses Hilfefenster

Beispiele:
- Speichere Bild Nummer 2 als /www/images/tree.jpg
gphoto -s 2 /www/images/tree.jpg

- Speichere Kleinbild 5 und Bild 5 als auto.jpg und auto-klein.jpg
gphoto -s 5 auto.jpg -t 5 auto-klein.jpg

- Speichere Bild 10 als meerschwein.tif und l�sche es dann
gphoto -s 10 meerschwein.tif -d 10

- Speichere eine Vorschau als roller.gif
gphoto -l roller.gif

Es ist leicht Shellskripte zu schreiben, die alle Aufgaben eines 
Digitalkameraprogramms erf�llen. Wenn Sie ein Skript schreiben, das Sie 
n�tzlich finden und mit andern die M�glichkeit geben m�chten es auch zu 
nutzen, dann schicken Sie, es an gphoto@gphoto.org.

Mehr Informationen

Die erste Stelle um Informationen �ber gPhoto zu bekommen, sollte die gPhoto 
Homepage http://www.gphoto.org sein.

E-Mail Unterst�tzung k�nnen Sie bekommen, indem Sie an gphoto@gphoto.org 
mailen. Das ist eine Mailing List, in welcher Sie sich auch eintragen k�nnen. 
Schauen Sie auf http://www.gphoto.org/lists.php3 und tragen Sie sich in der 
gPhoto Liste ein.

Senden Sie �nderungen und Korrekturen an:
Englische Orginalversion:
Autor: Scott Fritzinger (scott@unr.edu)
Datum: 24. Juni 1999
Aktualisiert: 25. Juni 1999

Deutsche Version:
Autor: Peter Gerbrandt (pgerbrandt@bfs.de)
Datum: 05. Dezember 2000
Aktualisiert: 11. Dezember 2000




